<!DOCTYPE html>
<html>

<head>
    <title>Xác nhận</title>
    <style>
        .wrap,
        body {
            background-color: white;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin-top: 20px;
        }

        .wrap {
            width: 510px;
            height: 550px;
            border: 1px solid blue;
            color: blue;
        }

        .regist_gender,
        .regist_major,
        .regist_date,
        .regist_address,
        .regist_image,
        .regist_name {
            background-color: #70AD47;
            color: white;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 90px;
            height: 15px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
            margin-right: 29px;
        }

        .button-wrap {
            border: 1px solid blue;
            background-color: #70AD47;
            color: white;
            padding: 5px;
            border-radius: 5px;
            width: 150px;
            height: 50px;
            margin-top: 20px;
            margin-left: 60px;
            cursor: pointer;
            border: none;
        }

        .image-container {
            width: 110px;
            height: 90px;
            overflow: hidden;
        }
       .image-container img {
            width: 100%;
            height: auto;
        }

        .image-label-container {
            display: flex;
            align-items: center;
        }
    </style>
</head>

<body>
    <div class="wrap">
        <form action="./database.php" method="post">
            <label for="account" class="regist_name">Họ và tên</label><?php echo $_POST["account"]; ?><br><br>
            
            <label for="gender" class="regist_gender">Giới tính</label>
            <?php
            $gender = array(
                "0" => "Nam",
                "1" => "Nữ"
            );
            $selectedGender = isset($_POST["gender"]) ? $_POST["gender"] : '';
            echo $gender[$selectedGender];
            ?><br><br>
            
            <label for="major" class="regist_major">Phân khoa</label>
            <?php
            $major = array(
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học dữ liệu"
            );
            $selectedMajor = isset($_POST["major"]) ? $_POST["major"] : '';
            echo $major[$selectedMajor];
            ?><br><br>

            <label for="birthdate" class="regist_date">Ngày sinh</label><?php echo $_POST["ngaysinh"]; ?><br><br>
        
            <label for="address" class="regist_address">Địa chỉ</label><?php echo $_POST["address"]; ?><br><br>
                
            <div class="image-label-container">
                <label for="image" class="regist_image">Hình ảnh</label>
                <div class="image-container">
                    <?php
                    if (isset($_FILES["image"]) && $_FILES["image"]["error"] == 0) {
                        $temp_image = $_FILES["image"]["tmp_name"];
                        $image_name = $_FILES["image"]["name"];
                        $image_path = "uploads_img/" . $image_name; 

                        move_uploaded_file($temp_image, $image_path);
                        echo '<img src="' . $image_path . '">';
                    } else {
                        echo "Ảnh tải lên bị lỗi.";
                    }
                    ?>
                </div><br><br>
            </div>

            <form action="database.php" method="post">
                <input type="hidden" name="account" value="<?php echo $_POST["account"]; ?>">
                <input type="hidden" name="gender" value="<?php echo $gender[$selectedGender]; ?>">
                <input type="hidden" name="major" value="<?php echo $major[$selectedMajor]; ?>">
                <input type="hidden" name="ngaysinh" value="<?php echo $_POST["ngaysinh"]; ?>">
                <input type="hidden" name="address" value="<?php echo $_POST["address"]; ?>">
                <input type="hidden" name="image" value="<?php echo $image_path; ?>">
                <button type="submit_xacnhan" class="button-wrap" id="xacnhan"> Xác nhận </button>
            </form>
        </form>
    </div>
</body>
</html>
