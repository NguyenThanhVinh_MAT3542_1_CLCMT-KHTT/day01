<?php
$servername = "localhost";
$database = "ltweb";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password, $database);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['reset'])) {
        $searchPhan_khoa = "";
        $searchHo_va_ten = "";
    } else {
        $searchPhan_khoa = $_GET['khoaInput'] ?? "";
        $searchHo_va_ten = $_GET['tuKhoaInput'] ?? "";
    }
} else {
    $searchPhan_khoa = "";
    $searchHo_va_ten = "";
}

$sql_select = "SELECT * FROM students WHERE Phan_khoa LIKE '%$searchPhan_khoa%' AND Ho_va_ten LIKE '%$searchHo_va_ten%'";
$result = $conn->query($sql_select);
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 10px;
            text-align: center; 
            vertical-align: middle; 
        }

        .label_search {
            background-color: #70AD47;
            color: white;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 90px;
            height: 15px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
        }

        .input_search {
            border: 1px solid blue;
            background-color: lightcyan;
            color: black;
            padding: 5px;
            border-radius: 0px;
            width: 290px;
            height: 25px;
            display: inline-block;
            text-align: left;
            margin: 5px;
        }

        .search {
            text-align: center;
        }

        .add {
            padding-left: 1308px;
        }

        .button-one{
            border: 1px solid blue;
            border-radius: 5px;
            background-color: blue;; 
            color: white;
            width: 90px;
            height: 30px;
            cursor: pointer;
        }

        .button-two{
            background-color: lightskyblue;; 
            color: white;
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="search">
        <label for="khoaInput" class="label_search">Khoa</label>
        <input type="text" id="khoaInput" name="khoaInput" class="input_search" value="<?php echo $searchPhan_khoa; ?>" required><br><br>

        <label for="tuKhoaInput" class="label_search">Từ khóa</label>
        <input type="text" id="tuKhoaInput" name="tuKhoaInput" class="input_search" value="<?php echo $searchHo_va_ten; ?>" required><br><br>
    </div>

    <div class="search">
        <button type="button"  id="resetbuton" class="button-one">Reset</button>
    </div>

    <p id="num_search">Số sinh viên tìm thấy:</p>

    <div class="add"><a href="register.php"><button class="button-one">Thêm</button></a></div>
    <table>
        <tr>
            <th>No</th>
            <th>Tên sinh viên</th>
            <th>Khoa</th>
            <th>Action</th>
        </tr>
    </table>
    <table id="table_res">
    </table>
    <script>
        $(document).ready(function () {
        function search() {
            var khoaInput = $('#khoaInput').val();
            var tuKhoaInput = $('#tuKhoaInput').val();
            $.ajax({
                type: 'GET',
                url: 'ajax.php',
                data: {
                    khoaInput: khoaInput,
                    tuKhoaInput: tuKhoaInput
                },
                success: function (data) {
                    $('#table_res').html(data);
                    
                    var num = $('#table_res tr').length;
                    $('#num_search').text('Số sinh viên tìm thấy: ' + num);
                }
            });
        }
        $('#khoaInput, #tuKhoaInput').on('keyup', function () {
            search();
        });
        $('#resetbuton').on('click', function () {
            $('#khoaInput, #tuKhoaInput').val('');
            search();
        });
        search();
    });
    </script>
</body>
</html>