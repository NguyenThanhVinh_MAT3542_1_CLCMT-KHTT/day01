<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .screen {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 150px; 
            height: 30px;      
            padding-top: 15px;     
            display: inline-block;
        }

        .input{
            padding-top: 2px;
            border: 1px solid blue;
            color: blue;
            padding: 5px; 
            border-radius: 0px;
            width: 150px;   
            height: 30px;        
            display: inline-block;
            margin: 5px;
            padding-bottom: 11px;
            padding-top: 9px;
        }

        p {
            background-color: lightgray; 
            color: black; 
            padding: 5px; 
            width: 325px;
            height: 20px;
            border-radius: 0px;           
            display: inline-block;  
        }

        .login {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            border-radius: 5px;
            width: 150px;   
            height: 50px;  
            margin-top: 20px; 
            margin-left: 100px;      
        }

        .outline {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 350px; 
            width: 600px;
            margin: 0; 
            background-color: white; 
            border: 1px solid blue;
            color: blue;
        }

        body {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 100vh; 
            margin: 0; 
            background-color: white; 
        }
        
    </style>
</head>

<body>
<div class="outline">
    <p> Bây giờ là:
        <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $gio = date('H:i ');
        echo $gio;

        $daysOfWeek = [
            1 => 'thứ 2',
            2 => 'thứ 3',
            3 => 'thứ 4',
            4 => 'thứ 5',
            5 => 'thứ 6',
            6 => 'thứ 7',
            7 => 'Chủ Nhật',
        ];
        
        $today = date('N');
        echo ', '.$daysOfWeek[$today];

        $ngay = date('d/m/Y');
        echo ' ngày '.$ngay;
        ?></p>

    <form action="login.php" method="POST">
        <label for="account" class="screen">Tên đăng nhập </label>
        <input type="text" id="account" name="account" class="input" required><br><br>

        <label for="password" class="screen">Mật khẩu </label>
        <input type="password" id="password" name="password" class="input" required><br><br>

        <button class="login"> Đăng nhập </button>
    </form>
</div>
</body>

</html>