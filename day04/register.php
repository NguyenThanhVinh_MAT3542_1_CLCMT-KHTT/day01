<!DOCTYPE html>
<html>
<head>
    <title>Đăng kí</title>
    <style>
        .form {
            display: flex;
            align-self: flex-start;
            padding-left: 20px;
        }

        .wrap,
        body {
            background-color: white;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin-top: 20px;
        }

        .wrap {
            width: 600px;
            height: 560px;
            border: 1px solid blue;
            color: blue;
        }

        .regist_gender,
        .regist_major,
        .regist_date,
        .regist_address,
        .regist_name {
            background-color: #70AD47;
            color: white;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 90px;
            height: 15px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
        }

        .take_name {
            border: 1px solid blue;
            background-color: white; 
            color: black;
            padding: 5px;
            border-radius: 0px;
            width: 400px;
            height: 25px;
            display: inline-block;
            text-align: left;
            margin: 5px;
        }

        .take_gender {
            color: black;
            padding-left: 10px;
            margin-left: -9px;
            margin-right: 10px;
            margin-top: 15px;
            font-size: 20px;
            display: inline-block;
        }

        .take_major {
            border: 1px solid blue;
            background-color: white; 
            color: black;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 191px;
            height: 36px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
            margin-left: 5px;
        }

        .take_date {
            border: 1px solid blue;
            background-color: white;
            color: black;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 180px;
            height: 34px;
            padding-top: 0px;
            padding-bottom: 2px;
            text-align: left;
            display: inline-block;
            margin-left: 5px;
        }   
        
        .take_address {
            border: 1px solid blue;
            background-color: white;
            color: black;
            border-radius: 0px;
            width: 186px;
            height: 92px;
            resize: vertical;
            padding-top: 0px;
            padding-bottom: 85px;
            display: inline-block;
            margin-left: 5px;
            padding-right: 222px;
            margin-top: 5px;
        }

        .button-wrap {
            border: 1px solid blue;
            background-color: #70AD47;
            color: white;
            padding: 5px;
            border-radius: 5px;
            width: 150px;
            height: 50px;
            margin-top: 20px;
            margin-left: 8px;
            cursor: pointer ;
            border: none;
        }

        .star{
            color: red;
        }

        #error {
            display: none;
            color: red;
            text-align: left; 
        }
    </style>

</head>
<body>
    <div class="wrap">
    <div id="error"></div>
        <form method="post" class = "form">
                <label for="account" class = "regist_name" >Họ và tên<span class="star">*</span></label>
                <input type="text" id="account" name="account" class = "take_name" required><br><br>
        </form>

        <form method="post" class="form">
            <label for="gender" class="regist_gender">Giới tính<span class="star">*</span></label>
            <label class="take_gender">
                <?php
                $gender = array(
                    "0" => "Nam",
                    "1" => "Nữ"
                );

                for ($i = 0; $i <= 1; $i++) {
                    echo '<input type="radio" name="gender" value="' . $i . '"> ' . $gender[$i] . ' ';
                }
                ?>
            </label>
            <br>
            <br>
        </form>

        <form method="post" class="form">
            <label for="major" class="regist_major">Phân khoa<span class="star">*</span></label>
            <select name="major" id="major" class="take_major">
                <option value="">--Chọn phân khoa--</option>
                <?php
                $major = array(
                    "MAT" => "Khoa học máy tính",
                    "KDL" => "Khoa học dữ liệu"
                );

                foreach ($major as $key => $value) {
                    echo '<option value="' . $key . '">' . $value . '</option>';
                }
                ?>
            </select>
            <br>
            <br>
        </form>

        <form method="post" class = "form">
            <label for="birthdate" class="regist_date">Ngày sinh<span class="star">*</span></label>
            <input type="text" id="ngaysinh" name="ngaysinh" class="take_date" placeholder="dd/mm/yyyy" required>
        </form>
        
        <form class="form">
            <label for="address" class="regist_address"> Địa chỉ </label>
            <textarea id="address" name="address" class="take_address"></textarea>
        </form>
        
        <button class = "button-wrap" id="enter"> Đăng ký </button>
    </div>
</body>
<script>
    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
    }

    function isValidDate(inputDate) {
        var dateParts = inputDate.split("/");
        if (dateParts.length !== 3) {
            return false;
        }

        var day = parseInt(dateParts[0], 10);
        var month = parseInt(dateParts[1], 10);
        var year = parseInt(dateParts[2], 10);

        if (month < 1 || month > 12 || day < 1 || day > 31) {
            return false;
        } else if (month === 2) {
            if ((isLeapYear(year) && day !== 29) || (day > 28 && !isLeapYear(year))) {
                return false;
            }
        } else if ([4, 6, 9, 11].includes(month)) {
            return day <= 30;
        }

        return true;
    }

    document.getElementById('enter').addEventListener('click', function() {
            var inputName = document.querySelector('.take_name');
            var genderInputs = document.querySelectorAll('input[name="gender"]');
            var selectedGender = Array.from(genderInputs).find(input => input.checked);
            var selectedFaculty = document.querySelector('.take_major');
            var inputDate = document.querySelector('.take_date');
            var error = document.getElementById('error');

            error.innerHTML = '';

            if (inputName.value.trim() === '') {
                error.innerHTML += 'Hãy nhập tên .<br>';
                error.style.paddingRight = '410px';
            }
            if (!selectedGender) {
                error.innerHTML += 'Hãy chọn giới tính .<br>';
                error.style.paddingRight = '410px';
            }
            if (selectedFaculty.value === '') {
                error.innerHTML += 'Hãy chọn phân khoa .<br>';
                error.style.paddingRight = '410px';
            }
            if (inputDate.value.trim() === '') {
                error.innerHTML += 'Hãy nhập ngày sinh .<br>';
                error.style.paddingRight = '410px';
            } else {
                var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
                if (!dateRegex.test(inputDate.value) || !isValidDate(inputDate.value)) {
                    error.innerHTML += 'Hãy nhập ngày sinh đúng định dạng .<br>';
                    error.style.paddingRight = '312px';
                }
            }

            if (error.innerHTML !== '') {
                error.style.display = 'block';
            } else {
                error.style.display = 'none';
            }
        });
</script>
</html>