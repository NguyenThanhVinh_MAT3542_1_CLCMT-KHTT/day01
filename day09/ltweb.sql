CREATE DATABASE `ltweb`;
CREATE TABLE `students` (
  `Ho_va_ten` text NOT NULL,
  `Gioi_tinh` text NOT NULL,
  `Phan_khoa` text NOT NULL,
  `Ngay_sinh` text NOT NULL,
  `Dia_chi` text NOT NULL,
  `Hinh_anh` text NOT NULL
)