<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $servername = "localhost";
    $database = "ltweb";
    $username = "root";
    $password = "";

    $conn = mysqli_connect($servername, $username, $password, $database);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $name = $_POST['account'];
    $gender = $_POST['gender'];
    $major = $_POST['major'];
    $birthday = $_POST['ngaysinh'];
    $address = $_POST['address'];
    $image_path = $_POST['image'];

    $sql = "INSERT INTO students (Ho_va_ten, Gioi_tinh, Phan_khoa, Ngay_sinh, Dia_chi, Hinh_anh) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssssss", $name, $gender, $major, $birthday, $address, $image_path);
    $stmt->execute();

    $sql_show = "SELECT * FROM students"; 
    $result = $conn->query($sql_show);

    $stmt->close();
    mysqli_close($conn);
}

header("Location: search.php");
exit();
?>