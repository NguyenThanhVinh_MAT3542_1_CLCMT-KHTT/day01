<?php
$servername = "localhost";
$database = "ltweb";
$username = "root";
$password = "";

$conn = new mysqli($servername, $username, $password, $database);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$searchPhan_khoa = $_GET['khoaInput'] ?? "";
$searchHo_va_ten = $_GET['tuKhoaInput'] ?? "";

$sql_select = "SELECT * FROM students WHERE Phan_khoa LIKE '%$searchPhan_khoa%' AND Ho_va_ten LIKE '%$searchHo_va_ten%'";
$result = $conn->query($sql_select);

if (isset($result) && $result->num_rows > 0) {
    $num = 1;
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $num . "</td>";
        echo "<td>" . $row["Ho_va_ten"] . "</td>";
        echo "<td>" . $row["Phan_khoa"] . "</td>";
        echo "<td>";
        echo "<button class='button-two' onclick=\"confirmDelete('" . $row["Ho_va_ten"] . "', '" . $row["Ngay_sinh"] . "')\">Xóa</button>&nbsp;";
        echo "<button class='button-two' onclick=\"redirectToUpdate('" . $row["Ho_va_ten"] . "', '" . $row["Ngay_sinh"] . "')\">Sửa</button>&nbsp;";
        echo "</td>";
        echo "</tr>";
        $num++;
    }
} else {
    echo "Không có dữ liệu sinh viên.";
}
?>
<script>
function confirmDelete(Ho_va_ten, Ngay_sinh) {
    if (confirm("Bạn muốn xóa sinh viên này?")) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "delete_students.php", true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    alert(xhr.responseText);
                    location.reload();
                } else {
                    alert('Đã có lỗi xảy ra khi xóa sinh viên!');
                }
            }
        };
        var data = "Ho_va_ten=" + encodeURIComponent(Ho_va_ten) + "&Ngay_sinh=" + encodeURIComponent(Ngay_sinh);
        xhr.send(data);
    }
}
function redirectToUpdate(Ho_va_ten, Ngay_sinh) {
    window.location = "update_students.php?Ho_va_ten=" + encodeURIComponent(Ho_va_ten) + "&Ngay_sinh=" + encodeURIComponent(Ngay_sinh);
}
</script>
