
<!DOCTYPE html>
<html>

<head>
    <title>Đăng ký</title>
    <style>
        .wrap,
        body {
            background-color: white;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin-top: 20px;
        }

        .wrap {
            width: 1100px;
            height: 740px;
            border: 1px solid blue;
            color: blue;
        }

        table {
            border-collapse: collapse;
            margin: 0 auto;
        }

        table, th, td {
            border: 1px solid black;
            width: 800px;
            height: 50px;
        }

        th, td {
            padding: 10px;
        }

        .button-wrap {
            text-align: center;
        }

        .button-wrap button {
            background-color: blue;
            color: white;
            padding: 15px 30px;
            font-size: 16px;
            border: none;
            border-radius: 7px;
            cursor: pointer;
        }

        .button-wrap button:hover {
            background-color: darkblue;
        }

        .input_name {
            border: 1px solid blue;
            background-color: white;
            color: black;
            padding: 5px;
            border-radius: 0px;
            width: 570px;
            height: 29px;
            display: inline-block;
            text-align: left;
        }

        .input_infor {
            border: 1px solid blue;
            background-color: white;
            color: black;
            padding: 5px;
            border-radius: 0px;
            width: 570px;
            height: 140px;
            display: inline-block;
            text-align: left;
        }

        .date-select {
            display: flex;
            gap: 10px;
            width: 570px;
            height: 27px;
        }

        .date-select select {
            width: 33.33%;
        }
    </style>
</head>

<body>
<div class="wrap">
    <form action="./regist_student.php" method="post" enctype="multipart/form-data">
        <h2 style="text-align: center;">Form đăng ký sinh viên</h2>
        <div id="error-message" style="color: red; text-align: center;"></div>
        <table>
            <tr>
                <th>Họ và tên</th>
                <td><input class ="input_name" type="text" name="account"></td>
            </tr>

            <tr>
                <th>Giới tính</th>
                <td>
                    <?php
                    $gender = array(
                        "0" => "Nam",
                        "1" => "Nữ"
                    );

                    for ($i = 0; $i <= 1; $i++) {
                        echo '<input type="radio" name="gender" value="' . $i . '"> ' . $gender[$i] . ' ';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <th>Ngày sinh</th>
                <td>
                    <div class="date-select">
                        <select name="year">
                        <option value="">Năm</option>
                            <?php
                            $currentYear = date("Y");
                            $startYear = $currentYear - 40;
                            $endYear = $currentYear - 15;
                            for ($i = $startYear; $i <= $endYear; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            }
                            ?>
                        </select>
                        <select name="month">
                        <option value="">Tháng</option>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            }
                            ?>
                        </select>
                        <select name="day">
                        <option value="">Ngày</option>
                            <?php
                            for ($i = 1; $i <= 31; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </td>
            </tr>

            <tr>
                <th>Địa chỉ</th>
                <td>
                <div class="date-select">
                    <select name="city" id="citySelect">
                        <option value="">Chọn thành phố</option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="Tp.Hồ Chí Minh">Tp.Hồ Chí Minh</option>
                    </select>

                    <select name="district" id="districtSelect" disabled>
                        <option value="">Chọn quận</option>
                    </select>
                </div>
                </td>
                </div>
            </tr>

            <tr>
                <th>Thông tin khác</th>
                <td>
                    <textarea class ="input_infor" name="other_info" rows="4" cols="50"></textarea>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" class="button-wrap">
                    <button type="submit" id="dangky">Đăng ký</button>
                </td>
            </tr>

        </table>
    </form>
</div>
</body>
<script>
    const citySelect = document.getElementById("citySelect");
    const districtSelect = document.getElementById("districtSelect");

    const cityDistricts = {
        "Tp.Hồ Chí Minh": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"],
        "Hà Nội": ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
    };

    citySelect.addEventListener("change", () => {
        const selectedCity = citySelect.value;
        districtSelect.innerHTML = '<option value="">Chọn quận</option>';
        districtSelect.disabled = true;

        if (cityDistricts[selectedCity]) {
            districtSelect.disabled = false;
            cityDistricts[selectedCity].forEach((district) => {
                const option = document.createElement("option");
                option.value = district;
                option.text = district;
                districtSelect.appendChild(option);
            });
        }
    });

    document.querySelector("form").addEventListener("submit", function (event) {
    const nameInput = document.querySelector('input[name="account"]');
    const genderInput = document.querySelectorAll('input[name="gender"]');
    const yearSelect = document.querySelector('select[name="year"]');
    const monthSelect = document.querySelector('select[name="month"]');
    const daySelect = document.querySelector('select[name="day"]');
    const citySelect = document.querySelector('select[name="city"]');
    const districtSelect = document.querySelector('select[name="district"]');

    let isError = false;
    let errorMessage = "";

    if (!nameInput.value) {
        isError = true;
        errorMessage += "Hãy nhập họ tên.<br>";
    }

    let genderSelected = false;
    for (let i = 0; i < genderInput.length; i++) {
        if (genderInput[i].checked) {
            genderSelected = true;
            break;
        }
    }
    if (!genderSelected) {
        isError = true;
        errorMessage += "Hãy chọn họ tên.<br>";
    }

    if (!yearSelect.value || !monthSelect.value || !daySelect.value) {
        isError = true;
        errorMessage += "Hãy chọn ngày sinh.<br>";
    }

    if (citySelect.value === "" || districtSelect.value === "") {
        isError = true;
        errorMessage += "Hãy chọn địa chỉ.<br>";
    }

    if (isError) {
        event.preventDefault();
        document.getElementById("error-message").innerHTML = errorMessage;
    }
});
</script>
</html>
