<!DOCTYPE html>
<html>
<head>
    <title>Đăng kí</title>
    <style>
        .wrap {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 400px; 
            width: 520px;
            margin: 0; 
            background-color: white; 
            border: 1px solid blue;
        }
        body {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 100vh; 
            margin: 0; 
            background-color: white; 
        }

        .regist_name {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 11px;
            border-radius: 0px;
            width: 90px; 
            height: 15px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block; 
        }

        .name{
            border: 1px solid blue;
            color: blue;
            padding: 5px; 
            border-radius: 0px;
            width: 320px;   
            height: 27px;        
            margin: 5px;
            margin-top: 47px;
            display: inline-block;
            
        }

        .regist_gender {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 11px;
            border-radius: 0px;
            width: 90px; 
            height: 15px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
        }

        .form {
            align-self: flex-start; 
            padding-left: 20px;
        }
        
        .take_gender{
            padding-left: 10px;
            color: black;
        }
        
        .regist_major {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 11px;
            border-radius: 0px;
            width: 90px; 
            height: 19px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
        }
        .take_major {
            border: 1px solid blue;
            background-color: white; 
            color: black; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 180px;
            height: 40px;     
            padding-top: 9px;
            padding-bottom: 11px;
            display: inline-block;
            text-align: center;     
            padding-left: 10px;
        }

        .button-wrap {
            border: 1px solid blue;
            background-color: #3CB371; 
            color: white; 
            padding: 5px; 
            border-radius: 5px;
            width: 150px;   
            height: 40px;
            margin-top: 11px; 
            margin-left: 27px;      
        }
    </style>

</head>
<body>
    <div class="wrap">
    <form method="post" class = "form">
            <label for="account" class = "regist_name" >Họ và tên </label>
            <input type="text" id="account" name="account" class = "name" required><br><br>
    </form>

    <form method="post" class="form">
        <label for="gender" class="regist_gender">Giới tính</label>
        <label class="take_gender">
            <?php
            $gender = array(
                "0" => "Nam",
                "1" => "Nữ"
            );

            for ($i = 0; $i <= 1; $i++) {
                echo '<input type="radio" name="gender" value="' . $i . '"> ' . $gender[$i] . ' ';
            }
            ?>
        </label>
        <br>
        <br>
    </form>

    <form method="post" class="form">
        <label for="major" class="regist_major">Phân khoa</label>
        <select name="major" id="major" class="take_major">
            <option value="">--Chọn phân khoa—</option>
            <?php
            $major = array(
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học dữ liệu"
            );

            foreach ($major as $key => $value) {
                echo '<option value="' . $key . '">' . $value . '</option>';
            }
            ?>
        </select>
        <br>
        <br>
    </form>

    <button class = "button-wrap"> Đăng ký </button>
    </div>
</body>
</html>