<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $servername = "localhost";
    $database = "ltweb";
    $username = "root";
    $password = "";

    $conn = mysqli_connect($servername, $username, $password, $database);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $name = $_POST['account'];
    $gender = $_POST['gender'];
    $major = $_POST['major'];
    $birthday = $_POST['ngaysinh'];
    $address = $_POST['address'];
    $image_path = $_POST['image'];

    $sql = "INSERT INTO students (Ho_va_ten, Gioi_tinh, Phan_khoa, Ngay_sinh, Dia_chi, Hinh_anh) VALUES (?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssssss", $name, $gender, $major, $birthday, $address, $image_path);
    $stmt->execute();

    $sql_show = "SELECT * FROM students"; 
    $result = $conn->query($sql_show);

    $stmt->close();
    mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <link rel="stylesheet" href="./style.css">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 10px; 
            text-align: center; 
            vertical-align: middle; 
        }

        .label_search {
            background-color: #70AD47;
            color: white;
            padding: 5px;
            margin: 5px;
            border-radius: 0px;
            width: 90px;
            height: 15px;
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;
            display: inline-block;
        }

        .input_search {
            border: 1px solid blue;
            background-color: lightcyan;
            color: black;
            padding: 5px;
            border-radius: 0px;
            width: 290px;
            height: 25px;
            display: inline-block;
            text-align: left;
            margin: 5px;
        }

        .search {
            text-align: center;
        }

        .add {
            padding-left: 1308px;
        }

        .button-one{
            border: 1px solid blue;
            border-radius: 5px;
            background-color: blue;; 
            color: white;
            width: 90px;
            height: 30px;
            cursor: pointer;
        }

        .button-two{
            background-color: lightskyblue;; 
            color: white;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="search">
        <label for="input" class="label_search">Khoa</label>
        <input type="text" name="input" class="input_search" required><br><br>

        <label for="input" class="label_search">Từ khóa</label>
        <input type="text" name="input" class="input_search" required><br><br>
    </div>

    <div class="search"><button class="button-one">Tìm kiếm</button></div>

    <p>Số sinh viên tìm thấy: </p>

    <div class="add"><a href="register.php"><button class="button-one">Thêm</button></a></div>
    
    <table>
        <tr>
            <th>No</th>
            <th>Tên sinh viên</th>
            <th>Khoa</th>
            <th>Action</th>
        </tr>
        <?php
        if (isset($result) && $result->num_rows > 0) {
            $num = 1;
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $num . "</td>";
                echo "<td>" . $row["Ho_va_ten"] . "</td>";
                echo "<td>" . $row["Phan_khoa"] . "</td>";
                echo "<td>";
                echo "<button class='button-two'>Xóa</button>&nbsp;";
                echo "<button class='button-two'>Sửa</button>";
                echo "</td>";
                echo "</tr>";
                $num++;
            }
        } else {
            echo "Không có dữ liệu sinh viên.";
        }
        ?>
    </table>   
</body>
</html>